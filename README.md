# README #

### Para que es el repositorio ###

Este repositorio es para que practiquemos con el Git y de paso vayamos creando algún tipo de documentación que nos sirva para el proyecto y para el grupo en general.

**TODOS TENEMOS QUE APORTAR ALGO, PARA EL QUE NO COMITEA HAY TABLA!!**

### ¿Cómo comenzar? ###

Bueno los que usan el SourceTree ya saben. Para los que les gusta la línea de comandos:

1. Se paran en el directorio donde quieren tener el repositorio y van a la parte izquierda de Bitbucket y copian el comando "git clone ..." ejemplo: git clone https://alealvUTN@bitbucket.org/alealvUTN/minipi_prueba.git. Ahora ya estan en el branch **Master** 

2. Tienen un solo archivo que se llama **Integrantes de MniPi.txt** completan sus datos ahí

3. Luego descargense los otros branches donde hice un par de carpetas

4. **Y por último y más importante vayan completando los archivos, modificando, haciendo commits, pusheando y probando. Si se mandan alguna cagada no hay problema!!!!**